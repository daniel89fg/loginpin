<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\LoginPin\Controller;

use FacturaScripts\Core\Base\Controller;
use FacturaScripts\Core\Model\User;
use FacturaScripts\Core\Model\RoleUser;
use FacturaScripts\Core\Model\Role;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Description of Login
 *
 * @author Daniel Fernández Giménez <daniel.fernandez@athosonline.com>
 */
class Login extends Controller {
    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $data = parent::getPageData();
        $data['menu'] = 'loginpin';
        $data['title'] = 'login';
        $data['icon'] = 'fas fa-user';
        $data['showonmenu'] = false;
        return $data;
    }
    
    /**
     * 
     * @param type $response
     */
    public function publicCore(&$response)
    {
        $this->response = &$response;
        $this->setTemplate('Login/Login');

        $password = $this->request->request->get('fsPassword');
        if ($password) {
            $this->newLogin($password);
        }
    }
    
    /**
     * Create new login users
     * 
     * @param type $password
     * @return type
     */
    private function newLogin($password)
    {
        $modelUser = new User();
        $where = [new DataBaseWhere('enabled', true)];

        $logged = [];
        foreach ($modelUser->all($where) as $user) {
            if ($user->verifyPassword($password)) {
                $logged[] = $user;
            }
        }

        if (empty($logged)) {
            $this->toolBox()->i18nLog()->warning('user-password-not-exist');
            return;
        } else if (count($logged) > 1) {
            $this->toolBox()->i18nLog()->warning('multiple-users-password');
            return;
        }

        $this->updateCookies($logged[0], true);
        $this->redirect(FS_ROUTE . '/' . $logged[0]->homepage);
    }

    /**
     * Updates user cookies.
     *
     * @param User $user
     * @param bool $force
     */
    private function updateCookies(User &$user, bool $force = false)
    {        
        if ($force || \time() - \strtotime($user->lastactivity) > 3600) {
            $ipAddress = $this->toolBox()->ipFilter()->getClientIp();
            if ($force) {
                $user->newLogkey($ipAddress);
            } else {
                $user->updateActivity($ipAddress);
            }

            $user->save();

            $expire = \time() + $this->timesession($user);
            $this->response->headers->setCookie(new Cookie('fsNick', $user->nick, $expire, FS_ROUTE));
            $this->response->headers->setCookie(new Cookie('fsLogkey', $user->logkey, $expire, FS_ROUTE));
            $this->response->headers->setCookie(new Cookie('fsLang', $user->langcode, $expire, FS_ROUTE));
            $this->response->headers->setCookie(new Cookie('fsCompany', $user->idempresa, $expire, FS_ROUTE));
        }
    }
    
    /**
     * Calculate time session expire
     * 
     * @param type $user
     * @return type
     */
    private function timesession($user)
    {
        $timesession = FS_COOKIES_EXPIRE;
        
        if (!$user->admin) {
            $modelRoleUser = new RoleUser();
            $whereRU = [new DataBaseWhere('nick', $user->nick)];
            
            $role = new Role();
            
            $rolesTimes = [];
            foreach ($modelRoleUser->all($whereRU) as $ru) {
                $role->clear();
                $role->loadFromCode($ru->codrole);
                
                if ($role->tiemposesion) {
                    $rolesTimes[] = $role->tiemposesion;
                }
            }
            
            if (count($rolesTimes) > 0) {
                sort($rolesTimes);
                $timesession = $rolesTimes[0] * 60;
            }
        }
        
        return $timesession;
    }
}
