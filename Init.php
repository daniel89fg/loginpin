<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\LoginPin;

use FacturaScripts\Core\Base\InitClass;

/**
 * Description of Init
 *
 * @author Daniel Fernández Giménez <daniel.fernandez@athosonline.com>
 */
class Init extends InitClass
{

    public function init()
    {
        $this->loadExtension(new Extension\Model\User());
    }

    public function update()
    {
        
    }
}