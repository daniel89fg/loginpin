<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\LoginPin\Extension\Model;

use FacturaScripts\Core\Model\User as parentUser;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

/**
 * Description of User
 *
 * @author Daniel Fernández Giménez <daniel.fernandez@athosonline.com>
 */
class User
{
    public function saveBefore()
    {
        return function() {
            if ($this->newPassword) {
                $model = new parentUser();
                $where = [new DataBaseWhere('nick', $this->nick, '!=')];

                $exist = false;
                foreach ($model->all($where) as $user) {
                    if (password_verify($this->newPassword, $user->password)) {
                        if (password_needs_rehash($this->newPassword, PASSWORD_DEFAULT)) {
                            $exist = true;
                        }
                    }
                }

                if ($exist) {
                    $this->toolBox()->i18nLog()->warning('user-password-exist');
                    return false;
                } 
            }
        };
    }
}